
# README #

This Repository Contains Support Files needed to Set an EC2 Instance with Galaxy Project

### Requirements ###

* Basic Knowledge of Ansible Scripts
* [Installation Tutorial for Reference](https://galaxyproject.github.io/training-material/topics/admin/tutorials/ansible-galaxy/tutorial.html)

### How do I get set up? ###

* git clone https://bitbucket.org/sshanbh1/galaxy-ec2-setup.git
* Go to the "Galaxy" folder.
* Check if Ansible is Installed by typing 'ansible' in the command prompt. 
* If not, follow the steps : [Installing Ansible on Ubuntu](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html#latest-releases-via-apt-ubuntu)
* Copy website certificates to "/etc/nginx/ssl/" folder.
* Edit "group_vars/galaxyservers.yml" change {{ PRIVATE KEY Content }} with the content inside the \*.key certificate file.
* Edit hosts file and replace "usegalaxy.bioviz.org" with the host you want the website to be hosted.
* First Run "ansible-galaxy install -p roles -r requirements.yml"
* This will download all the roles required for the Ansible and Galaxy Setup - [From Ansible Galaxy](https://galaxy.ansible.com/)
* Run "sudo ansible-playbook galaxy.yml"

In the end you should see the output like
```
RUNNING HANDLER [restart galaxy] ****************************************
changed: [galaxy.example.org]
```
If there are any errors contant the Developer or refer [Installation Guide](https://galaxyproject.github.io/training-material/topics/admin/tutorials/ansible-galaxy/tutorial.html)

### How do I update the galaxy playbook to use the latest galaxy version? ###
* go to this link https://galaxyproject.github.io/training-material/topics/admin/tutorials/ansible-galaxy/tutorial.html#requirements
* Find the Hands:On Installing roles section in the requirements section
* Under the second point, Create a requirements.yml file, find the version of src: galaxyproject.galaxy
* Sample snippet - src: galaxyproject.galaxy
 				   version: 0.9.6
* Update the local latest galaxyproject.galaxy by changing its version (to the latest version found in the above step) in requirements.yml (assuming you have one) at the root of your Ansible playbook, remove roles/galaxyproject.galaxy, and run the ansible-galaxy role install -p roles -r requirements.yml
* Incase of any issues or errors, contact the following people or drop a message to the galaxy admins at gitter. https://gitter.im/galaxyproject/admins#
### Who do I talk to? ###

* Sameer Shanbhag [sshanbh1@uncc.edu](mailto:sshanbh1@uncc.edu)
* Dr. Loraine [Ann.Loraine@uncc.edu](mailto:Ann.Loraine@uncc.edu)
* Chaitanya Kintali [ckintali@uncc.edu](mailto:ckintali@uncc.edu)
